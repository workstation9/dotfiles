alias wacom-list='xsetwacom --list devices'
alias wacom-monitor='xsetwacom set "Wacom Intuos S Pen stylus" MapToOutput 3440x1440+1920+0'
alias wacom-monitor-left='xsetwacom set "Wacom Intuos S Pen stylus" MapToOutput 1720x1440+1920+0'
alias wacom-monitor-right='xsetwacom set "Wacom Intuos S Pen stylus" MapToOutput 1720x1440+3640+0'
alias wacom-built='xsetwacom set "Wacom Intuos S Pen stylus" MapToOutput 1920x1080+0+360'
alias wacom-auxiliar='xsetwacom set "Wacom Intuos S Pen stylus" MapToOutput 1920x1080+5360+180'
alias zshconfig="subl ~/.zshrc && source ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
alias ll="colorls -latrh --gs"
alias kl="k -Ahrt"
alias pls="sudo"

alias python="python3"
alias docker="podman"

alias upgrade="sudo apt update && apt list --upgradable && sudo apt upgrade && flatpak update -y"
alias fichar="cd ~/Documents/Proyectos/utils/FichadorParadigma && pipenv run python fichador_horas.py && cd ${OLDPWD}"
alias marp-convert-all="marp --pdf --allow-local-files --html -I . -o output; marp --pptx --allow-local-files --html -I . -o output"
alias marp-convert="marp --pdf --allow-local-files --html -I . -o output"
alias buy-ps5='cd /home/david/Documents/Proyectos/utils/Amazon-AutomatedBuy && pipenv run python main.py && cd /home/david'
alias whats-monitor='cd /home/david/Documents/Proyectos/utils/WaMonitor && nohup pipenv run python src/telegramRPC.py &; watch -d -t "sqlite3 log.db \"select * from stats\" | tail -n10"'
alias gitlab-ci='docker run --rm -v $PWD:$PWD -w $PWD -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest exec docker '

function telgm {
  local message="${@}"
  curl -s -X POST https://api.telegram.org/bot"1694332145:AAH9Y7RvflpezPpuXeNP0nPdACY2u7csLoI"/sendMessage -d chat_id="6430104" -d text="${message}" >/dev/null
}

git config --global credential.helper store
